$: << File.dirname(__FILE__)

require 'config'
#require 'lib/verifiers'
require 'packages/update'
require 'packages/ntp'
require 'packages/git'
require 'packages/host'
require 'packages/elastic_search'
require 'packages/postgres'
require 'packages/imagemagick'
require 'packages/exiftool'
require 'packages/deploy'
#require 'packages/utilities'
require 'packages/ssh'
require 'packages/nginx'
require 'packages/app'
require 'packages/ruby_install'
require 'packages/openjdk7' 
require 'packages/firewall_rules'

ASSETS_PATH = File.join(File.dirname(__FILE__), 'assets')

policy :stack, :roles => :app do
  requires :ssh           # packages/ssh ## We want to close the root account _fast_
  requires :system_update # packages/update
  requires :firewall_rules
  requires :time_sync     # packages/ntp
  requires :scm           # packages/git
  requires :host          # packages/host
  requires :ruby          # packages/ruby_install
  requires :elastic_search # packages/elastic_search
  requires :database      # packages/postgres
  requires :imagemagick
  requires :exiftool 
# requires :tools         # packages/utilities
#
  requires :deployer      # packages/deploy
#  # requires :node          # packages/node
#  requires :java_runtime  # packages/openjdk7 ## required from elastic_search
  requires :webserver     # packages/nginx
  requires :app           # packages/app
end

deployment do
  
  delivery :capistrano do
    set :user, ROOT_USER
    set :password, ROOT_USER_PASSWORD # During deploy, your public_key is added to the root user,
				      # and password-based login is disabled 
    role :app, HOSTIP, :primary => true
  end

  source do
    prefix   '/usr/local'           # where all source packages will be configured to install
    archives '/usr/local/sources'   # where all source packages will be downloaded to
    builds   '/usr/local/build'     # where all source packages will be built
  end
end
