My use of sprinkle to create private images 

##Instructions
    
Copy config.rb.example to config.rb. Update it with your settings.

Then run the following:

    bundle install
    bundle exec sprinkle -v -c -s system_setup.rb 

After about 15 - 20 minutes, you're ready to deploy.
