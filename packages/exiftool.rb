package :exiftool do 
  description 'add exiftool binaries'

  apt 'libimage-exiftool-perl'

  verify do
    has_executable "exiftool"
  end
end
