package :imagemagick do 
  description 'add imagemagick binaries'

  apt 'imagemagick'

  verify do
    has_executable "mogrify"
  end
end
