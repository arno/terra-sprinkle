package :ruby_build, :provides => :ruby do
  description 'Install ruby from source'
  
  requires :install_ruby, :add_gems
end

package :install_ruby do
  description 'Ruby Virtual Machine'
  
  requires :ruby_build_dependencies
  
  source "ftp://ftp.ruby-lang.org/pub/ruby/1.9/ruby-1.9.3-p194.tar.gz" 
  
  verify do
    has_executable 'ruby'
    ruby_can_load 'rubygems'
  end
end

package :add_gems do
  description 'Gems for Ruby'

  gem 'bundler'
  gem 'foreman'

  requires :install_ruby

  verify do
    ruby_can_load 'bundler'
    ruby_can_load 'foreman' 
  end
end

package :ruby_build_dependencies do
  description "dependencies for building Ruby from source"

  apt %w( gcc build-essential openssl libreadline6 libreadline6-dev
    curl zlib1g zlib1g-dev libssl-dev libyaml-dev
    libxml2-dev libxslt1-dev autoconf libc6-dev 
    libncurses5-dev automake libtool bison make )

   #git libsqlite3-dev  
end
