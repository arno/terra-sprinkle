package :openjdk7, :provides => :java_runtime do

  description "Open JDK Runtime environment"

  apt "openjdk-7-jre"

  verify do
    has_apt "openjdk-7-jre"
  end

end
