package :nginx, :provides => :webserver do
  description 'Nginx Web Server'

  requires :nginx_package, :nginx_sites_folders, :nginx_remove_default_config, :nginx_app_config
end

package :nginx_package do
  apt 'nginx' 
  
  verify do
    has_apt 'nginx'
  end
end

package :nginx_sites_folders do
  requires :nginx_package

  runner '/etc/init.d/nginx stop'
  runner '/etc/init.d/nginx start'

  verify do
    has_directory '/etc/nginx/sites-available'
    has_directory '/etc/nginx/sites-enabled'
    has_process 'nginx'
  end

end

package :nginx_remove_default_config do

  runner "rm -f /etc/nginx/sites-available/default"
end


package :nginx_app_config do
  requires :nginx_package, :nginx_sites_folders

  config_file = "/etc/nginx/sites-available/#{APP_NAME}.conf"
  config_template = ERB.new(File.read(File.join(File.join(File.dirname(__FILE__), '..', 'assets'), 'nginx-app.conf.erb')),0).result 

  runner "rm -f #{config_file} /etc/nginx/sites-enabled/#{APP_NAME}"

  push_text config_template, config_file

  
  runner "ln -s /etc/nginx/sites-available/#{APP_NAME}.conf /etc/nginx/sites-enabled/#{APP_NAME}"
#
  runner '/etc/init.d/nginx restart'

  # We can regenerate the config on every provisioning, and it doesn't hurt.
  # And it's much better than verifying line by line :)
  # 
  #verify do
  #  has_file "/etc/nginx/sites-available/#{APP_NAME}.conf"
  #  has_file "/etc/nginx/sites-enabled/#{APP_NAME}"
  #end
end
