package :elastic_search do
  description 'Elastic Search'

  requires :java_runtime

  version='0.90.3'

  runner "wget --quiet https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-#{version}.deb"
  runner "dpkg --install elasticsearch-#{version}.deb"
  runner "rm elasticsearch*.deb"

  noop do
    post :install, '/etc/init.d/elasticsearch restart'
  end
  
  verify do
    has_apt "elasticsearch"
  end
end
