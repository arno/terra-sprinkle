package :ntp, :provides => :time_sync do

  description "NTP client to prevent time skew issues."

  apt %w( ntp )

  verify do
    has_apt "ntp"
  end

end
