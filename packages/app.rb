package :rails_app, :provides => :app do
  description "Finalize settings for the rails app"

  requires :app_dir, :known_hosts
end

package :app_dir do
  description 'Create the application directory'
  runner "mkdir -p /srv/applications/#{APP_NAME}"
  runner "chown -R '#{DEPLOY_USER}' /srv/applications/#{APP_NAME}"
  
  verify do
    has_directory "/srv/applications/#{APP_NAME}"
    belongs_to_user("/srv/applications/#{APP_NAME}", DEPLOY_USER)
  end
end

package :known_hosts do
  description "Add git host to known_hosts so that capistrano doesn't prompt for it"
  
  # create known hosts ..
  runner "ssh-keyscan #{GIT_HOST} | sudo -u #{DEPLOY_USER} tee -a /home/#{DEPLOY_USER}/.ssh/known_hosts"
  
  verify do
    file_contains "/home/#{DEPLOY_USER}/.ssh/known_hosts", GIT_HOST
  end
end
