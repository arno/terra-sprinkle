package :update, :provides => :system_update do
  description "System Update"
  
  requires :apt_update, :install_unattended_upgrades, :use_sudo => true
end

package :apt_update do
  runner "apt-get update"
end

package :install_unattended_upgrades do
  description "install package to stay secure"
  
  apt 'unattended-upgrades' do
    post :install, 'unattended-upgrade'
  end

  verify do
    has_executable 'unattended-upgrades'
  end

end
