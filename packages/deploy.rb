package :prepare_deploy, :provides => :deployer do
  description 'Create deploy user'
  
  requires :create_deploy_user, :add_deploy_ssh_keys, :set_permissions , :create_deploy_location

#    :install_procfile, 
end

package :create_deploy_user do
  description "Create the deploy user"
  
  runner "useradd --create-home --shell /bin/bash --user-group --groups users,sudo #{DEPLOY_USER}"
  runner "echo '#{DEPLOY_USER}:#{DEPLOY_USER_PASSWORD}' | chpasswd"
  runner "echo '#{DEPLOY_USER}\tALL=(ALL:ALL) ALL' | tee -a /etc/sudoers"
  runner "echo '#{DEPLOY_USER}\tALL=(root) NOPASSWD:/sbin/stop' | tee -a /etc/sudoers"
  runner "echo '#{DEPLOY_USER}\tALL=(root) NOPASSWD:/sbin/start' | tee -a /etc/sudoers"
  
  verify do
    has_directory "/home/#{DEPLOY_USER}"
  end
end

package :add_deploy_ssh_keys do
  description "Add deployer public key to authorized ones"
  requires :create_deploy_user
  
  id_rsa_pub = `cat ~/.ssh/id_rsa.pub`
  authorized_keys_file = "/home/#{DEPLOY_USER}/.ssh/authorized_keys"
  
  push_text id_rsa_pub, authorized_keys_file do
    # Ensure there is a .ssh folder.
    pre :install, "mkdir -p /home/#{DEPLOY_USER}/.ssh"
  end
  
  verify do
#    file_contains authorized_keys_file, id_rsa_pub
   has_file authorized_keys_file
  end
end

package :set_permissions do
  description "Set correct permissions and ownership"
  requires :add_deploy_ssh_keys
  
  runner "chmod 0700 /home/#{DEPLOY_USER}/.ssh"
  runner "chown -R #{DEPLOY_USER}:#{DEPLOY_USER} /home/#{DEPLOY_USER}/.ssh"  
  runner "chmod 0700 /home/#{DEPLOY_USER}/.ssh/authorized_keys"
end

package :create_deploy_location do
  description "create locations to deploy the application"

  requires :create_app_log_location

  verify do
    has_directory "/srv/applications/#{APP_NAME}"
  end
end

package :create_app_log_location do
  description "create log location"

  runner "mkdir -p /var/log/#{APP_NAME}"
  runner "chown -R #{DEPLOY_USER}:root /var/log/#{APP_NAME}"

  verify do
    has_directory "/var/log/#{APP_NAME}"
  end

end

#package :install_procfile do
#  description "Installs procfile for foreman to use"
#  requires :create_deploy_user
#
#  procfile = "/home/#{DEPLOY_USER}/Procfile"
#  #Shouldn't this just be the procfile from the repo?
#
#  transfer PROCFILE , procfile
#
#  verify do
#    has_file procfile
#  end
#end
