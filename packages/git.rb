package :git, :provides => :scm do
  description 'Git'
  apt 'git'
    
  verify do
    has_executable 'git'
  end
end
