package :configure_ssh, :provides => :ssh do
  description "Configure SSH"
  
  requires :ssh_keys, :ssh_config, :sshd_config#, :ssh_restart
end

package :ssh_keys do
  description "Generate default SSH key"

  requires :deployer 
  runner %Q{ ssh-keygen -t rsa -N '' -f /home/#{DEPLOY_USER}/.ssh/id_rsa -C "#{DEPLOY_USER}@#{HOSTIP}" }   
  runner "chown -R #{DEPLOY_USER}:#{DEPLOY_USER} /home/#{DEPLOY_USER}/.ssh" 

  noop do
    post :install, '/etc/init.d/ssh reload'
  end

  verify do
    has_file "/home/#{DEPLOY_USER}/.ssh/id_rsa"
    has_file "/home/#{DEPLOY_USER}/.ssh/id_rsa.pub"
    belongs_to_user "/home/#{DEPLOY_USER}/.ssh/*", DEPLOY_USER
  end
end
#
package :ssh_config do
  description "Default SSH config"

  requires :root_has_my_public_key

  config_file = '/etc/ssh/ssh_config'
  config_template = File.join(File.dirname(__FILE__), '..', 'assets', 'ssh_config')
#  
  transfer config_template, config_file do
    post :install, "reload ssh"
  end
#  
  #verify do
  #  file_contains config_file, $(head -n 1 #{config_template})
  #end
end

package :sshd_config do
  description "Default SSHD config"
#  
  config_file = '/etc/ssh/sshd_config'
  config_template = File.join(File.dirname(__FILE__), '..', 'assets', 'sshd_config')
#  
  transfer config_template, config_file do
    post :install, "/etc/init.d/ssh reload"
  end
#  
  #verify do
  #  file_contains config_file, $(head -n 1 #{config_template})
  #end
end

package :root_has_my_public_key do
  description "Don't be locked out of the root account" 
 
  id_rsa_pub = `cat ~/.ssh/id_rsa.pub`
  authorized_keys_file = "/root/.ssh/authorized_keys"
  
  push_text id_rsa_pub, authorized_keys_file do
    # Ensure there is a .ssh folder.
    pre :install, "mkdir -p /root/.ssh"
  end
end

#%w[start stop restart reload].each do |command|
#  package :"ssh_#{command}" do
#    runner "/etc/init.d/ssh #{command}"
#  end
#end
#
## package :ssh_custom_port do
##   replace_text 'Port 22', 'Port 2500', '/etc/ssh/sshd_config', :=> true
## end

