package :firewall_rules do
  description "close our ports for the outside world"

  firewall_rules = ERB.new(File.read(File.join(File.join(File.dirname(__FILE__), '..', 'assets'), 'firewall_rules.sh.erb'))).result
  
  target = "/root/firewall.sh"

  runner "rm -f #{target}"
  runner "touch #{target}"
  runner "chmod +x #{target}"
  push_text firewall_rules, target

  runner "/root/firewall.sh"

  # no verifyer here: We want to recompile and execute the 
  # firewall on every reprovisioning

end
