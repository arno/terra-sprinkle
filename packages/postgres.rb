package :postgres, :provides => :database do
  description 'PostgreSQL database'

  requires :postgres_package, :postgres_user , :postgres_create_production_database, :postgres_config_for_capistrano
end

package :postgres_package do
  apt 'postgresql' 
  apt 'postgresql-client' 
  apt 'libpq-dev'
  apt 'postgresql-contrib'
  
  verify do
    has_apt 'postgresql'
    has_apt 'postgresql-client'
    has_apt 'libpq-dev'
  end
end
  
package :postgres_user do
  runner %{echo "CREATE ROLE #{DEPLOY_USER} WITH SUPERUSER LOGIN PASSWORD '#{DEPLOY_POSTGRES_PASSWORD}';" | sudo -u postgres -i psql}
 
  runner %{echo "GRANT ALL PRIVILEGES ON DATABASE #{DB_NAME} to #{DEPLOY_USER}"}
  
  # TODO: check for privileges
  verify do
    @commands << "echo 'SELECT ROLNAME FROM PG_ROLES' | sudo -u postgres -i psql | grep -q #{DEPLOY_USER}"
  end
end

package :postgres_create_production_database do
  runner "sudo -u postgres -i createdb --owner=#{DEPLOY_USER} #{DB_NAME}"

  verify do
    @commands << "sudo -u postgres -i psql -l | grep -q #{DB_NAME}"
  end  
end

package :postgres_config_for_capistrano do
  description "put database connection details in deploy-user home directory"

  requires :create_deploy_user
  target = "/home/#{DEPLOY_USER}/database.yml"
  database_yml_template = ERB.new(File.read(File.join(File.join(File.dirname(__FILE__), '..', 'assets'), 'database.yml.erb'))).result
  
  runner "rm -f #{target}"
  runner "touch #{target}"
  push_text database_yml_template, target

  verify do
    has_file "/home/#{DEPLOY_USER}/database.yml"
  end

end

# package :postgres_autostart do
#   description "PostgreSQL: Autostart on reboot"
#   requires :postgres_core
  
#   runner '/usr/sbin/update-rc.d postgresql-8.4 defaults'
# end
